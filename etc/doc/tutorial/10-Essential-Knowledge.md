# Essential Knowledge

This section will cover some very useful - in fact *essential* - knowlege
for getting the most of your Sonic Pi experience.

We'll cover how to take advantage of the many keyboard shortcuts
available to you, how to share your work and some tips on performing
with Sonic Pi.
